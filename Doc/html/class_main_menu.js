var class_main_menu =
[
    [ "MainMenu", "class_main_menu.html#a53eecf9d5ffd094f54ac4193e7e57eaf", null ],
    [ "~MainMenu", "class_main_menu.html#a0a19ddba3ac52bf39c09b579171c98f2", null ],
    [ "executeFrame", "class_main_menu.html#ac5cf8bad04644e021a6462807db04fe9", null ],
    [ "getResult", "class_main_menu.html#afe638e269dfc5ee9831e58558fad19f8", null ],
    [ "handleClick", "class_main_menu.html#a19e08f7eb24a60bb39ac4e354337afaa", null ],
    [ "initializeFrame", "class_main_menu.html#a51ee4b0576cd8af58c29ae55e03a3ca6", null ],
    [ "_exitButton", "class_main_menu.html#a1d5b8e85122195e9271c88ada201e500", null ],
    [ "_playButton", "class_main_menu.html#ac05ae4932a0352072d22b90b1acf098d", null ],
    [ "_settingsButton", "class_main_menu.html#a047faf40717c8579075c487801d37268", null ]
];