#include "LevelSerialization.hpp"

#include <vector>
#include "../../modele/Level/Tile.hpp"
#include <iterator>
#include "../../modele/Level/Tileset.hpp"
#include "../../util/TilesetManager.hpp"

Level LevelSerialization::loadLevelFromFile(std::string path)
{
    //Open the file
    Json::Value root;
    std::ifstream config_doc(path, std::ifstream::binary);
    config_doc >> root;

    //Create the level object
    Level l("Level1",root["height"].asInt(),root["width"].asInt(),root["tileheight"].asInt());

    //Loop on the layers
    const Json::Value layers = root["layers"];
    for ( int i = 0; i < layers.size(); i++ )
    {
        //Get the layer data
        std::vector<Tile*> layerTiles;
        for(int y = 0; y <root["height"].asInt();y++)
        {
            for(int x = 0; x <root["width"].asInt();x++)
            {
                int data = layers[i]["data"][x+y*root["width"].asInt()].asInt();
                if(data != 0)
                {
                    layerTiles.push_back(new Tile(x,y,data));
                }
            }
        }

        std::string name = layers[i]["name"].asString();
        //Add the layer to the level
        l.addLayer(layerTiles,name);
    }

    //Loop on all the tilesets and add them to the tileset manager
    const Json::Value tileSets = root["tilesets"];
    for ( int i = 0; i < tileSets.size(); i++ )
    {
        std::string tileSetPath = tileSets[i]["source"].asString();
        //Replace the .tsx extension (from Tiled) by a .png extension
        tileSetPath.erase(tileSetPath.length()-3).append("png");

        TilesetManager::getInstance()->addTileset(new Tileset(tileSets[i]["firstgid"].asInt(),tileSetPath));
    }
    std::cout << l.str() << std::endl;

    return l;
}
