#include "GobSerialization.hpp"

GameObject* GobSerialization::loadGobFromFile(std::string path)
{
    //Open the file
    Json::Value root;
    std::ifstream config_doc(path, std::ifstream::binary);
    config_doc >> root;

    //Create the gameobject
    GameObject* g = new GameObject(root["name"].asString());

    //Loop on all the components
    const Json::Value components = root["components"];
    for ( int i = 0; i < components.size(); i++ )
    {
        //Create the component
        if(components[i]["name"].asString() == "renderer")
        {
            RendererComponent* r = (RendererComponent*) g->addComponentOfType(ComponentType::E_RendererComponent);
            r->setPosition(components[i]["pos_x"].asInt(),components[i]["pos_y"].asInt());
            r->setRotation(components[i]["rotation"].asFloat());
            r->setTexture(components[i]["imagePath"].asString(),components[i]["width"].asInt(),components[i]["height"].asInt());
        }

        if(components[i]["name"].asString() == "boxCollider2D")
        {
            BoxCollider2D* b = (BoxCollider2D*) g->addComponentOfType(ComponentType::E_BoxCollider2D);
            b->getRect().top = components[i]["y"].asInt();
            b->getRect().left = components[i]["x"].asInt();
            b->getRect().width = components[i]["width"].asInt();
            b->getRect().height = components[i]["height"].asInt();

            b->setIsTrigger(components[i]["isTrigger"].asBool());
            CollisionSystem::getInstance()->addCollider(b);
        }

        if(components[i]["name"].asString() == "physicsBody")
        {
            g->addComponentOfType(ComponentType::E_PhysicsBody);
        }

        if(components[i]["name"].asString() == "playerController")
        {
            g->addComponentOfType(ComponentType::E_PlayerController);
        }

        if(components[i]["name"].asString() == "animator")
        {
            g->addComponentOfType(ComponentType::E_Animator);
        }

        if(components[i]["name"].asString() == "enemyController")
        {
            g->addComponentOfType(ComponentType::E_EnemyController);
        }

        if(components[i]["name"].asString() == "projectile")
        {
            g->addComponentOfType(ComponentType::E_Projectile);
        }
    }

    return g;
}
