//! The tileset manager.
/*! The tileset manager is a singleton class that manage the tilesets used by the levels. */
#ifndef TILESETMANAGER_HPP_INCLUDED
#define TILESETMANAGER_HPP_INCLUDED

#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include <vector>
#include "../modele/Level/Tileset.hpp"
#include <iostream>

class TilesetManager
{
public:
    //! The singleton getter.
    /*! Create the instance if needed, then return it. */
    static TilesetManager* getInstance()
    {
        if(_instance == nullptr)
        {
            _instance = new TilesetManager();
        }

        return _instance;
    }
    //! The method used to add tilesets.
    /*! This method is used to add tilesets to the manager. */
    void addTileset(Tileset* tileset);
    //! The sprite getter.
    /*! This method return the right sprite according to the level gid given. */
    sf::Sprite* getSpriteFromGid(const int gid, const int tileSize);
    //! The destructor.
    /*! Destroy all the tilesets hold by the manager. */
    virtual ~TilesetManager();
private:
    //! Constructor.
    /*! Private constructor to respect the Singleton pattern. */
    TilesetManager() {};
    //! The manager instance.
    /*! The static instance of this class returned when asked for. */
    static TilesetManager* _instance;
    //! The tilesets.
    /*! All the tilesets used by the level. */
    std::vector<Tileset*> _tileSets;
    //! The textures.
    /*! All the textures of the sprites returned. We need to keep them into memory or they won't be displayed. */
    std::vector<sf::Texture> _textures;
};

#endif // TILESETMANAGER_HPP_INCLUDED
