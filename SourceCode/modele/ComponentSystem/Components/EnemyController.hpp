//! Component that define the logic of an enemy
/*!
    It holds the component logic that define an enemy
*/

#ifndef ENEMYCONTROLLER_HPP_INCLUDED
#define ENEMYCONTROLLER_HPP_INCLUDED

#include "IComponent.hpp"
#include "../GameObject/GameObject.hpp"

#define MOVE_FORCE_ENEMY 90

class EnemyController :
    public IComponent
{
    public:
        //! Constructor
        /*! Instantiate the component */
        EnemyController(IGameObject* owner): IComponent(owner,ComponentType::E_EnemyController){};
        //!Destructor
        /*! Prevents memory leaks */
        virtual ~EnemyController(){};
        //!Copy constructor
        /*!Create a new Instance by copying another.*/
        EnemyController(const EnemyController& other);
        //! Operator =
        /*! Redefine the equal operator to adapt it for this class. */
        EnemyController& operator=(const EnemyController& rhs);
        //! Clone
        /*! Clone the instance by creating a new instance that contains the same values of the original one. */
        IComponent* clone(IGameObject* owner);
        //! On Collision Event
        /*! Receive the informations when a collision happen and react.
            When a collision with a projectile happen, the enemy disappear.
        */
        void onCollision(IComponent& other);
        //! On receive message event
        /*! Handle message reception. */
        void receiveMessage(const std::string& message){};
        //! Start event
        /*! Handle Start event. */
        void start();
        //!Update Event
        /*! Handle update event. */
        void update(const sf::Time& deltaTime);

    private:
        //! The Direction of the enemy
        /*! Used to change the direction of enemy. */
        int _dir = 1;
};

#endif // ENEMYCONTROLLER_HPP_INCLUDED
