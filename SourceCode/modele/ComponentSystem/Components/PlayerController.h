//! Component that define the controls.
/*! It holds the logic to define the controls for the player entity. */

#ifndef PLAYERCONTROLLER_H
#define PLAYERCONTROLLER_H

#include "IComponent.hpp"
#include "../GameObject/GameObject.hpp"
#include "Projectile.h"
#include "RendererComponent.hpp"
#include "BoxCollider2D.hpp"

#define JUMP_FORCE 20
#define MOVE_FORCE 250

class PlayerController :
    public IComponent
{
    public:
        //! Constructor
        /*! Instantiate the component.*/
        PlayerController(IGameObject* owner): IComponent(owner,ComponentType::E_PlayerController){};
        //! Destructor
        /*! Prevents memory leaks. */
        virtual ~PlayerController(){};
        //! Copy Constructor
        /*! Create a new instance by copying another. */
        PlayerController(const PlayerController& other);
        //! Operator =
        /*! Redefine the equal operator to adapt it for this class. */
        PlayerController& operator=(const PlayerController& rhs);
        //! CLone
        /*! Clone the instance by creating a new instance that contains the same values. */
        IComponent* clone(IGameObject* owner);
        //! Projectile setter
        /*! Used to set the projectile. */
        void setProj(IGameObject* p){ this->proj =  p;}
        //! On Collision event.
        /*! Handle collision events. */
        void onCollision(IComponent& other){};
        //! Receive Message event.
        /*! Receive message event handler. */
        void receiveMessage(const std::string& message){};
        //! Start event.
        /*! Handle start event. */
        void start();
        //! Update Event
        /*! Handle update event. */
        void update(const sf::Time& deltaTime);

    private:
        //! The Direction of the player
        /*! Used to change the direction of player. */
        int _dir = 1;
        //! The projectile object
        /*! Used to change the projectile's state. */
        IGameObject* proj;
        //! The projectile renderer
        /*! Used to move the projectile renderer. */
        RendererComponent* projRenderer;
        //! The projectile collider
        /*! Used to move the projectile collider. */
        BoxCollider2D* projCollider;
        //! The projectile.
        /*! Used to change the direction of the projectile. */
        Projectile* projProj;
        //! The player renderer
        /*! used to move the player. */
        RendererComponent* _renderer;
};

#endif // PLAYERCONTROLLER_H
