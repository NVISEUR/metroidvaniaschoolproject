#include "Animator.hpp"

Animator::Animator(const Animator& other) : IComponent(other)
{
    this->_remainingFrame = other._remainingFrame;
    this->_currentAnim = other._currentAnim;
    this->_currentIndex = other._currentIndex;
}

Animator& Animator::operator=(const Animator& rhs)
{
    if (this == &rhs) return *this;

    IComponent::operator=(rhs);

    this->_remainingFrame = rhs._remainingFrame;
    this->_currentAnim = rhs._currentAnim;
    this->_currentIndex = rhs._currentIndex;

    return *this;
}

IComponent* Animator::clone(IGameObject* owner)
{
    Animator* comp = new Animator(*this);
    comp->_gameobject = owner;
    return comp;
}

void Animator::update(const sf::Time& deltaTime)
{
    IComponent::update(deltaTime);
}

void Animator::receiveMessage(const std::string& message)
{
    _remainingFrame--;

    if(_remainingFrame > 0)
        return;

    std::istringstream iss(message);
    std::vector<std::string> results(std::istream_iterator<std::string>{iss},
    std::istream_iterator<std::string>());

    std::string type = results[0];

    if(type == "State")
    {
        bool grounded = results[1] != "0";
        float velocityX = std::stof(results[2]);
        float velocityY = std::stof(results[3]);
        Animations anim;

        if(grounded && velocityX == 0 && velocityY == 0)
        {
            anim = Animations::idle;
            _currentAnimFrameNbr = IDLE_FRAME_NUMBER;
        }
        else if(!grounded && velocityY != 0)
        {
            anim = Animations::jumping;
            _currentAnimFrameNbr = JUMPING_FRAME_NUMBER;
        }
        else if(grounded && velocityX != 0)
        {
            anim = Animations::running;
            _currentAnimFrameNbr = RUNNING_FRAME_NUMBER;
        }

        if(_currentAnim != anim)
        {
            _currentAnim = anim;
            _currentIndex = 0;
        }
        else
        {
            _currentIndex += 1;

            if(_currentIndex >= _currentAnimFrameNbr)
            {

                _currentIndex = 0;
            }
        }

        std::stringstream ss;
        ss << "ChangeAnim " << _currentIndex << " " << _currentAnim;

        this->getGameObject()->sendMessage(ss.str());

        _remainingFrame = FRAME_BETWEEN_ANIM;
    }
}
