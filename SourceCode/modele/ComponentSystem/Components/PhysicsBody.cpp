#include "PhysicsBody.hpp"
#include <string>
#include <algorithm>
#include <iterator>

PhysicsBody::PhysicsBody(const PhysicsBody& other): IComponent(other)
{
    this->_velocityX = other._velocityX;
    this->_velocityY = other._velocityY;
}

PhysicsBody& PhysicsBody::operator=(const PhysicsBody& rhs)
{
    if (this == &rhs) return *this;

    IComponent::operator=(rhs);

    this->_velocityX = rhs._velocityX;
    this->_velocityY = rhs._velocityY;
    return *this;
}

IComponent* PhysicsBody::clone(IGameObject* owner)
{
    PhysicsBody* comp = new PhysicsBody(*this);
    comp->_gameobject = owner;
    return comp;
}

void PhysicsBody::receiveMessage(const std::string& message)
{
    std::istringstream iss(message);
    std::vector<std::string> results(std::istream_iterator<std::string>{iss},
    std::istream_iterator<std::string>());

    std::string type = results[0];

    if(type == "AddForce")
    {
        int valuex = std::stoi(results[1]);
        int valuey=0;
        if(this ->_grounded)
        {
            valuey =  std::stoi(results[2]);
            if(valuey != 0)
                this ->_grounded = false;
        }
        addForce(valuex,valuey);
    }
}

void PhysicsBody::update(const sf::Time& deltaTime)
{
    IComponent::update(deltaTime);

    if(_grounded)
    {
        if(!CollisionSystem::getInstance()->isPointColliding(this->_collider->getRect().left,this->_collider->getRect().top+this->_collider->getRect().height+5)
        && !CollisionSystem::getInstance()->isPointColliding(this->_collider->getRect().left+this->_collider->getRect().width,this->_collider->getRect().top+this->_collider->getRect().height+5))
        {
            _grounded = false;
        }
    }

    //raise y velocity by the gravity if in the air
    if(!_grounded)
    {
        addForce(0,GRAVITY* deltaTime.asSeconds());
    }

    bool collision = false;

    std::stringstream ss;
    ss << "State " << _grounded << " " << _velocityX << " " << _velocityY;

    this->getGameObject()->sendMessage(ss.str());

    //First move in the x axis, and then resolve collision in this axis
    if(_velocityX != 0)
    {
        int dir = _velocityX > 0 ? 1 : -1;

        _velocityX *= deltaTime.asSeconds();

        if(dir == 1)
        {
            if(this->_velocityX >= MAX_VELOCITY_X)
                this->_velocityX = MAX_VELOCITY_X;
        }
        else
        {
            if(this->_velocityX <= MAX_VELOCITY_X*-1)
                this->_velocityX = MAX_VELOCITY_X*-1;
        }
        translateAll(_velocityX, 0);

        collision = CollisionSystem::getInstance()->isColliding(*this->_collider);

        if(collision)
            resolveCollision(true);
        _velocityX = 0;
    }

    //Then move in the y axis, and resolve collision too
    if(_velocityY != 0)
    {
        int dir = _velocityY > 0 ? 1 : -1;

        if(dir == 1)
        {
            if(this->_velocityY >= MAX_VELOCITY_Y)
                this->_velocityY = MAX_VELOCITY_Y;
        }

        translateAll(0, _velocityY);

        collision = CollisionSystem::getInstance()->isColliding(*this->_collider);

        if(collision)
        {
            resolveCollision(false);
            if(dir == 1)
                _grounded = true;
            _velocityY = 0;
        }
    }
}

void PhysicsBody::resolveCollision(bool xAxis)
{
    int dir;

    if(xAxis)
        dir = _velocityX > 0 ? 1 : -1;
    else
        dir = _velocityY > 0 ? 1 : -1;

    //We are moving away from the collision, so we want to reverse the direction
    dir *= -1;

    //While we are colliding, move away pixel by pixel until we are not anymore
    while(CollisionSystem::getInstance()->isColliding(*this->_collider))
    {
        if(xAxis)
        {
            translateAll(1*dir, 0);
        }
        else
        {
            translateAll(0, 1*dir);
        }
    }
}

void PhysicsBody::translateAll(float x, float y)
{
    std::stringstream ss;
    ss <<"TranslateAll " << x << " "<<y;
    this -> _gameobject->sendMessage(ss.str());
    this->_collider->translate(x,y);
}

void PhysicsBody::start()
{
    this->_collider = (BoxCollider2D*) ((GameObject*)this->_gameobject)->getComponentOfType(ComponentType::E_BoxCollider2D);

    if(this->_collider == nullptr)
        std::cout << "Error : the physicsBody component can't get the collider" << std::endl;
}

PhysicsBody::~PhysicsBody()
{
    delete this->_renderer;
    delete this->_collider;
}
