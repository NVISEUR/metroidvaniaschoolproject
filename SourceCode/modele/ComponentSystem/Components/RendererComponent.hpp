//! Componenet that define the rendering of an entity.
/*!
    It holds the logic that define the entities rendering.
*/

#ifndef RENDERERCOMPONENT_HPP_INCLUDED
#define RENDERERCOMPONENT_HPP_INCLUDED

#include "IComponent.hpp"

class RendererComponent :
	public IComponent
{
public:
    //! Constructor
    /*! Instantiate the component. */
	RendererComponent(IGameObject* owner) : IComponent(owner,ComponentType::E_RendererComponent) {};
	//! Destructor
	/*! Prevents memory leaks. */
	virtual ~RendererComponent() {};
	//! Copy Constructor
	/*! Create a new instance by copying another. */
	RendererComponent(const RendererComponent& other);
	//! Operator =
	/*! Redefine the equal operator to adapt it for this class. */
	RendererComponent& operator=(const RendererComponent& rhs);
	//! Clone
	/*! Clone the instance by creating a new one that contains the same values.*/
	IComponent* clone(IGameObject* owner);

	//! On receive message event.
	/*! Handle message reception. */
	void receiveMessage(const std::string& message);
	//! On Start Event
	/*! Handle Start event*/
	void start(){};
	//! On Update event
	/*! Handle Update event. */
	void update(const sf::Time& deltaTime);
	//! On Collision Event
	/*! Handle collision event. */
	void onCollision(IComponent& other) {};
	//! Position Setter
	/*! Sets the x and y position of the entity's renderer.
        It Should only be used when placing the game object in the world, never to move it (use the physics velocity).
    */
	void setPosition(const float x,const float y);
	//! Rotation Setter
	/*! Sets the rotation of the entity's Renderer.*/
	void setRotation(const float r);
	//! Scale Setter
	/*! Sets the scale of the entity's renderer. */
	void setScale(const float x,const float y);
	//! Texture Setter
	/*! Sets the texture of the renderer. */
	void setTexture(const std::string& path, const int width = 0, const int height = 0);
	//! Sprite Setter
	/*! Sets the renderer's sprite. */
	void setSprite(const sf::Sprite& sprite, const int overrideWidth, const int overrideHeight);
    //! Translation of the renderer.
    /*! Translate the x and y of the render. */
	void translate(const float x, const float y);
	//! Position getter
	/*! Return the position of the renderer.*/
	sf::Vector2<float> getPosition() {return _position;}
	//! Sprite Getter
	/*! Return the sprite used by the renderer. */
	sf::Sprite* getSprite() {return &_sprite;}

private:
    //! Position Vector
    /*! the position of the renderer. */
    sf::Vector2<float> _position;
    //! Rotation value
    /*! Rotation of the renderer.*/
    float _rotation = 0;
    //! Renderer Scale
    /*! The scale of the vector. */
    sf::Vector2<float> _scale;
    //! Renderer Texture
    /*! The texture of the renderer. */
    sf::Texture _texture;
    //! Sprite
    /*! Sprite. */
    sf::Sprite _sprite;
    //! Direction of the renderer.
    /*! Direction of the renderer. */
    int _dir = 1;
    //! Width of the renderer.
    /*! Width of the renderer. */
    int _width;
    //! Height of the renderer.
    /*! Height of the Renderer. */
    int _height;
};

#endif
