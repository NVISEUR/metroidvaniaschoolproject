#include "PlayerController.h"

PlayerController::PlayerController(const PlayerController& other):IComponent(other)
{
}

PlayerController& PlayerController::operator=(const PlayerController& rhs)
{
    if (this == &rhs) return *this;
    IComponent::operator=(rhs);
    return *this;
}

IComponent* PlayerController::clone(IGameObject* owner)
{
    PlayerController* comp = new PlayerController(*this);
    comp->_gameobject = owner;
    return comp;
}

void PlayerController::start()
{

    this->_renderer = ((RendererComponent*)((GameObject*)this->_gameobject)->getComponentOfType(ComponentType::E_RendererComponent));
    this->projCollider = ((BoxCollider2D*)((GameObject*)this->proj)->getComponentOfType(ComponentType::E_BoxCollider2D));
    this->projRenderer = ((RendererComponent*)((GameObject*)this->proj)->getComponentOfType(ComponentType::E_RendererComponent));
    this->projProj = ((Projectile*)((GameObject*)this->proj)->getComponentOfType(ComponentType::E_Projectile));

    if(this->projProj == nullptr)
        std::cout << "error";
}

void PlayerController::update(const sf::Time& deltaTime)
{
    IComponent::update(deltaTime);
    float x=0,y=0;
    std::stringstream ss;

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
    {
        x-=MOVE_FORCE;
        if(_dir != -1)
        {
            _dir = -1;
            std::stringstream sss;
            sss << "ChangeDir " << _dir;
            this->_gameobject->sendMessage(sss.str());
        }
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        x+=MOVE_FORCE;
        if(_dir != 1)
        {
            _dir = 1;
            std::stringstream sss;
            sss << "ChangeDir " << _dir;
            this->_gameobject->sendMessage(sss.str());
        }
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
    {
        y+= -JUMP_FORCE;
    }

    if(x!=0 || y!=0 )
    {
     ss << "AddForce " << x <<" "<< y;
     this->_gameobject->sendMessage(ss.str());
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::RShift) && !this->proj->isActivated())
       {
           this->proj->activate();
           if(_dir == 1)
           {
               this->projRenderer->setPosition(this->_renderer->getPosition().x+55,this->_renderer->getPosition().y);
               this->projCollider->setPosition(this->_renderer->getPosition().x+56,this->_renderer->getPosition().y);
           }
           else
           {
               this->projRenderer->setPosition(this->_renderer->getPosition().x-6,this->_renderer->getPosition().y);
               this->projCollider->setPosition(this->_renderer->getPosition().x-7,this->_renderer->getPosition().y);
           }
           this->projProj->setDir(_dir);
       }
}


