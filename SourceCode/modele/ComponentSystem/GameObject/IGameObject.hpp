//! Game object Interface
/*! Interface that specify the method behavior that each game object should implement. */
#ifndef IGAMEOBJECT_HPP_INCLUDED
#define IGAMEOBJECT_HPP_INCLUDED

#include <string>

class IGameObject
{
public:
    //! SendMessage Event
    /*! should be used to broadcast a message to all the components of the game object. */
	virtual void sendMessage(const std::string& message) const = 0;
	//! Abstract activate event
	/*! should be used to activate all the components of the game object. */
	virtual void activate() =0;
	//! Abstract deactivate event
	/*! should be used to deactivate all the components of the game object. */
	virtual void deactivate() =0;
	//! Abstract State getter
	/*! Game object state getter */
	virtual bool isActivated() = 0;
protected:
    //! The state of the game object instance
    /*! Boolean that indicates if the current game object is active. */
    bool _activated=true;
};

#endif // IGAMEOBJECT_HPP_INCLUDED
