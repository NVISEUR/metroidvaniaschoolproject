#include "GameObject.hpp"

GameObject::GameObject(const GameObject& other)
{
    for (int i = 0; i < other._components.size(); i++)
	{
        this->_components.push_back(other._components[i]->clone(this));
	}

    this->_name = other._name;
}

GameObject& GameObject::operator=(const GameObject& rhs)
{
    if (this == &rhs) return *this;

    for (int i = 0; i < rhs._components.size(); i++)
	{
        this->_components.push_back(rhs._components[i]->clone(this));
	}

    this->_name = rhs._name;

    return *this;
}

IComponent* GameObject::getComponentOfType(const ComponentType type)
{
    for(IComponent *component : _components)
    {
        if(component->getType() == type)
        {
            return component;
        }
    }

    return nullptr;
}

bool GameObject::removeComponentOfType(const ComponentType type)
{
    for (int i = 0; i < _components.size(); i++)
	{
        if(_components[i]->getType() == type)
        {
            delete _components[i];
            _components.erase(_components.begin()+i);
            return true;
        }
	}

    return false;
}

IComponent* GameObject::addComponentOfType(const ComponentType type)
{
    IComponent* component;

    switch(type)
    {
    case ComponentType::E_RendererComponent:
            component = new RendererComponent(this);
        break;

    case ComponentType::E_BoxCollider2D:
            component = new BoxCollider2D(this);
        break;

    case ComponentType::E_PhysicsBody:
            component = new PhysicsBody(this);
        break;

    case ComponentType::E_PlayerController:
            component = new PlayerController(this);
        break;

    case ComponentType::E_Animator:
            component = new Animator(this);
        break;
    case ComponentType::E_EnemyController:
            component = new EnemyController(this);
        break;
    case ComponentType::E_Projectile:
        component = new Projectile(this);
        break;
    }

    _components.push_back(component);

    return component;
}

sf::Sprite* GameObject::update(const sf::Time& deltaTime)
{
    sf::Sprite* s = nullptr;

	for (int i = 0; i < _components.size(); i++)
	{
        if(_components[i]->getType() == ComponentType::E_RendererComponent)
        {
            s = ((RendererComponent*)_components[i])->getSprite();
        }
        else
        {
            _components[i]->update(deltaTime);
        }
	}

	return s;
}

void GameObject::start()
{
	for (int i = 0; i < _components.size(); i++)
	{
        _components[i]->start();
	}
}

void GameObject::sendMessage(const std::string& message) const
{
	for (int i = 0; i < _components.size(); i++)
	{
        _components[i]->receiveMessage(message);
	}
}

GameObject::~GameObject()
{
    for (int i = 0; i < _components.size(); i++)
	{
        delete _components[i];
	}

	_components.clear();
}

void GameObject::activate(){
    	for (int i = 0; i < _components.size(); i++)
	{
        _components[i]->activate();
	}
	this->_activated = true;
}

void GameObject::deactivate(){
    	for (int i = 0; i < _components.size(); i++)
	{
        _components[i]->deactivate();
	}
	this->_activated = false;
}

void GameObject::onCollision(IComponent& other)
{
    for (int i = 0; i < _components.size(); i++)
	{
        _components[i]->onCollision(other);
	}
}
