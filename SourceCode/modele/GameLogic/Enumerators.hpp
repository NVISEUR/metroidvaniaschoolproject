#ifndef ENUMERATORS_HPP_INCLUDED
#define ENUMERATORS_HPP_INCLUDED

//! Enum representing all components.
/*! Hold all the possible component types. */
enum ComponentType
{
    E_RendererComponent,
    E_BoxCollider2D,
    E_PhysicsBody,
    E_PlayerController,
    E_Animator,
    E_EnemyController,
    E_Projectile
};

//! Enum representing the menu state
/*! Hold all the possible views the game can display. */
enum MenuResult
{
    play,
    aboutUs,
    exitGame,
    none
};

//! Enum representing a game state.
/*! Hold all the possible game state the game can be in. */
enum GameState
{
    Uninitialized,
    ShowingMenu,
    ShowingAboutUs,
    Playing,
    Exiting
};

//! Enum representing the animation states.
/*! Hold all the possible animation states. */
enum Animations
{
    running = 0,
    jumping = 2,
    idle = 3
};
#endif
