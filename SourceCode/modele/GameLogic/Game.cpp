#include "Game.hpp"
#include "Enumerators.hpp"
#include <math.h>
#include "../../vue/Scenes/MainMenu.hpp"
#include "../../vue/Scenes/PlayGround.hpp"

//Initialized here since it is a static member.
Game* Game::_instance = nullptr;

Game* Game::getInstance()
{
    if(Game::_instance == nullptr)
    {
        Game::_instance = new Game();
    }

    return Game::_instance;
}

void Game::start(const int width, const int height, const int bitsPerPixel,const std::string& windowTitle)
{
    if(_state != Uninitialized)
    {
        return;
    }

    _mainWindow.create(sf::VideoMode(width,height,bitsPerPixel),windowTitle);
    _mainWindow.setFramerateLimit(60);

    if(_fpsFont.loadFromFile("Ressources/font/arial.ttf"))
    {
        _fpsText = sf::Text("0 fps",_fpsFont,30);
        _fpsText.setPosition(0,0);
        _fpsText.setFillColor(sf::Color::Black);
    }
    else
    {
        std::cout << "couldn't load font" << std::endl;
    }

    changeState(GameState::ShowingMenu);

    gameLoop();
}

void Game::gameLoop()
{
    //Initialize the clock
	sf::Clock deltaClock;
	//Main game loop
    while (_mainWindow.isOpen())
    {
        //Initialize the scene if it has not been initialized
        if(!_initScene)
        {
            this->_currentScene->initializeFrame(_mainWindow);
            this->_initScene = true;
        }

        //Clear the window
        _mainWindow.clear(sf::Color(0,128,128));
        //Execute the scene frame
        _currentScene->executeFrame(_mainWindow, _deltaTime);
        //drawFps();

        //Display the scene
        _mainWindow.display();
        //Restart the clock
        _deltaTime = deltaClock.restart();
    }
}

void Game::drawFps()
{
    float framerate = 1/_deltaTime.asSeconds();
    _fpsText.setString(std::to_string(int(round(framerate))) + " fps");
    _mainWindow.draw(_fpsText);
}

void Game::changeState(const GameState state)
{
    if(this->_currentScene != nullptr)
        delete _currentScene;


    this->_initScene = false;


    switch(state)
    {
    case ShowingMenu:
        _currentScene = new MainMenu();
        break;

    case ShowingAboutUs:
        _currentScene = new AboutUs();
        break;

    case Playing:
        _currentScene = new PlayGround();
        break;

    case Exiting:
        _mainWindow.close();
        break;
    }

    this->_state = state;
}
