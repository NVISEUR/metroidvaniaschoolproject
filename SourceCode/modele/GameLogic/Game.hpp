//! Represent the center of the game.
/*!
    Hold the entire logic that link the different classes together,
    such as choosing which frame to show, ...
*/
#ifndef GAME_HPP_INCLUDED
#define GAME_HPP_INCLUDED

#include <array>
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
#include "Enumerators.hpp"
#include "../ComponentSystem/GameObject/GameObject.hpp"
#include "../../vue/Scenes/IScene.hpp"
#include "CollisionSystem.hpp"
#include "../../vue/Scenes/AboutUS.h"

class Game
{
public:
    //! Return the Singleton instance of the Game class.
    /*! Return the Singleton instance of the Game class, or create a new one if it does not exist yet. */
    static Game* getInstance();
    //! Start the game.
    /*! Start the game, showing the window to the player and launching the game loop. */
    void start(const int width, const int height, const int bitsPerPixel,const std::string& windowTitle);
    //! Change the state of the game.
    /*! Change the state of the game, changing at the same time which IFrame is being rendered on the screen. */
    void changeState(const GameState state);
    //! Destructor.
    /*! Prevents memory leaks. */
    virtual ~Game() { delete _currentScene; };
private:
    //! Constructor.
    /*! Private constructor to respect the Singleton pattern. */
    Game(){};
    //! Represent the game loop.
    /*! Hold the entire logic behind the game loop, such as rendering, ... */
    void gameLoop();
    //! Exit the game.
    /*! Exit the game, close the window and make sure to clear every pointers. */
    void exit();
    //! Draw the fps on the screen
    /*! Calculate the frame per seconds based on the delta time, and draw it on the screen.*/
    void drawFps();
    //! The game instance.
    /*! The static instance of this class returned when asked for. */
    static Game* _instance;
    //! The current game state.
    /*! The current state of the game. Changing it will change what is shown on the screen, and also how the game react to the user input. */
    GameState _state = Uninitialized;
    //! The main window.
    /*! The main window. Can be sent to the different frame so that they can choose what to draw on it. */
    sf::RenderWindow _mainWindow;
    //! The current IScene.
    /*! The current scene showing on the screen. */
	IScene* _currentScene = nullptr;
    //! The delta time.
    /*! The time between the end of the two last frame. */
	sf::Time _deltaTime = sf::Time::Zero;
	//! The fps
	/*! The fps text that is shown on the screen. */
	sf::Text _fpsText;
    //! The fps text font
	/*! The fps text font that is shown on the screen. */
	sf::Font _fpsFont;
    //! The Initial Scene
    /*! Boolean that indicate if the current scene has been initialized or not */
	bool _initScene = false;
};

#endif // GAME_HPP_INCLUDED
