//! Define a level.
/*! Define the informations of a level */

#ifndef LEVEL_HPP_INCLUDED
#define LEVEL_HPP_INCLUDED

#include "Layer.hpp"
#include <vector>
#include <array>
#include <sstream>
#include "Tileset.hpp"

class Level
{
public:
    //! Constructor
    /*! Instantiate the level. */
    Level(const std::string& name = "Undefined", const int height = 0, const int width = 0, const int tileSize = 0)
    : _name(name), _height(height), _tileSize(tileSize), _width(width) {};
    //! Destructor
    /*! Prevents memory leaks. */
    virtual ~Level();
    //! Copy Constructor
    /*! Create a new Instance by copying another. */
    Level(const Level& other);
    //! Operator =
    /*! Redefine the equal operator to adapt it for this class. */
    Level& operator=(const Level&  rhs);
    //! Adds a layer to the level
    /*! Adds a layer composed by tiles to the level. */
    void addLayer(const std::vector<Tile*>& tiles, const std::string& name);
    //! Layer number getter
    /*! Gets the number of layers in the level */
    int getLayersNumber() {return _layers.size();}
    //! Tile size getter
    /*! Gets the level tile's size*/
    int getTileSize() {return _tileSize;}
    //! Layer getter
    /*! Gets the layer by it's index. */
    Layer& getLayerByIndex(int index) {return *_layers[index];}
    //! Level name getter
    /*! Gets the name of the level. */
    std::string& getName() {return this->_name;}
    //! To String
    /*! Returns the level informations by string. */
    std::string str();

private:
    //! The layer list.
    /*! Vector of layers that compose the level. */
	std::vector<Layer*> _layers;
    //! The level name
    /*! The level name. */
	std::string _name;
    //! The level height
    /*! The level height. */
    int _height;
    //! The level width
    /*! The level width. */
    int _width;
    //! The tile size
    /*! The tile's size.*/
    int _tileSize;
};

#endif // LEVEL_HPP_INCLUDED
