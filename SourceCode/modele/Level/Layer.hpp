//! Define a layer
/*!
    It holds the logic that define a layer.
*/
#ifndef LAYER_HPP_INCLUDED
#define LAYER_HPP_INCLUDED

#include <iostream>
#include <vector>
#include "Tile.hpp"

class Layer
{
public:
    //! Constructor
    /*! Instantiate the layer without tiles.*/
    Layer(const std::string& name = "Undefined") {};
    //! Constructor
    /*! Instantiate the layer with tiles.*/
    Layer(const std::vector<Tile*>& tiles, const std::string& name = "Undefined")
          : _tiles(tiles), _name(name) {};
    //! Destructor
    /*! Prevents memory leak. */
    virtual ~Layer();
    //! Copy constructor
    /*! Create a new Instance by copying another. */
    Layer(const Layer& other);
    //! Operator =
    /*! Redefine the equal operator to adapt it for this class. */
    Layer& operator=(const Layer& rhs);
    //! Name getter
    /*! Returns the name of the layer.*/
    std::string getName() { return _name; };
    //! Layer getter
    /*! Returns the tile list of the layer. */
    const std::vector<Tile*>& getTiles() {return _tiles; };
private:
    //! Tile vector
    /*! the tiles that compose the layer. */
    std::vector<Tile*> _tiles;
    //! Layer name
    /*! The layer name. */
    std::string _name;
};

#endif // LAYER_HPP_INCLUDED
