#include <iostream>
#include "Button.hpp"

Button::Button(const int x, const int y, const int width, const int height, const std::string& imagePath, const std::string& text)
{
    //Create the rect of the button
    _rect = sf::IntRect(x,y,width,height);

    //Create the sprite of the button
    if(_texture.loadFromFile(imagePath))
    {
        _sprite.setTexture(_texture);
        _sprite.setScale(width/_sprite.getLocalBounds().width,
                      height/_sprite.getLocalBounds().height);
    }
    else
    {
        std::cout << "couldn't load image" << std::endl;
    }

    //Create the text of the button
    if(_font.loadFromFile("Ressources/font/arial.ttf"))
    {
        _text = sf::Text(text,_font,30);
    }
    else
    {
        std::cout << "couldn't load font" << std::endl;
    }

    setPos(x, y);
}

void Button::setPos(const int x, const int y)
{
    _rect = sf::IntRect(x,y,_rect.width,_rect.height);
    _sprite.setPosition(x,y);
    _text.setPosition(sf::Vector2f(x + _rect.width/2 - _text.getGlobalBounds().width / 2, y + _rect.height/2 - _text.getGlobalBounds().height / 2));
}

void Button::draw(sf::RenderWindow& window)
{
    window.draw(_sprite);
    window.draw(_text);
}
