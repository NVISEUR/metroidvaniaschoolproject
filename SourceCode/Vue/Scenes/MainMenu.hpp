//! The MainMenu scene.
/*! Shows buttons allowing the users to move between the different scenes. */
#ifndef MAINMENU_HPP_INCLUDED
#define MAINMENU_HPP_INCLUDED

#include "../SceneComponents/Button.hpp"
#include "IScene.hpp"
#include "../../modele/GameLogic/Game.hpp"

class MainMenu : public IScene
{
public:
    //! Constructor.
    /*! Does not do anything for this class. */
	MainMenu(){};
    //! Initialize the scene.
    /*! Create and configure the objects that will be shown on the screen. */
    void initializeFrame(sf::RenderWindow& window);
    //! Draw the scene on the screen.
    /*! Draw the scene on the screen.\n
    As this is a menu, the scene will wait for the user inputs and not return until a button is clicked.*/
    void executeFrame(sf::RenderWindow& window,const sf::Time& deltaTime);
    //! Destructor.
    /*! Destroy the buttons that make the scene. */
	virtual ~MainMenu();

private:
    //! Play button.
    /*! Move the users to the PlayGround scene. */
    Button* _playButton;
    //! About us button.
    /*! Move the users to the AboutUs scene. */
    Button* _aboutUsButton;
    //! Exit button.
    /*! Close the game. */
    Button* _exitButton;

    //! Return the result of the menu.
    /*! Wait for the user input, then return which button has been clicked. */
    MenuResult getResult(sf::RenderWindow& window);
    //! Handle a click on the screen.
    /*! Handle the users click on the screen, and give back which button as been clicked. */
    MenuResult handleClick(const int x,const int y);

};

#endif // MAINMENU_HPP_INCLUDED
