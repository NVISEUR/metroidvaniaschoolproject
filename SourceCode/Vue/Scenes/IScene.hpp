//! The IScene interface.
/*! The IScene interface represent a scene of our game. */
 #ifndef ISCENE_HPP_INCLUDED
#define ISCENE_HPP_INCLUDED

#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
#include "../../modele/GameLogic/Enumerators.hpp"

class IScene
{
public:
    //! The scene initialization.
    /*! The scene initialization should be used to initialize all the components of the scene. */
    virtual void initializeFrame(sf::RenderWindow& window) = 0;
    //! The scene execution.
    /*! The scene execution should hold the behavior of the scene that should happen every frame. */
    virtual void executeFrame(sf::RenderWindow& window, const sf::Time& deltaTime) = 0;
};

#endif // ISCENE_HPP_INCLUDED
