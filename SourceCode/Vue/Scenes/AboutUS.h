//! The AboutUs scene.
/*! Shows details about the creators of the game, and the context in which the game was made. */
#ifndef ABOUTUS_H
#define ABOUTUS_H

#include "../SceneComponents/Button.hpp"
#include "IScene.hpp"
#include "../../modele/GameLogic/Game.hpp"

class AboutUs : public IScene
{
    public:
        //! Initialize the scene.
        /*! Create and configure the objects that will be shown on the screen. */
        void initializeFrame(sf::RenderWindow& window);
        //! Draw the scene on the screen.
        /*! Draw the scene on the screen.\n
        As this is a menu, the scene will wait for the user inputs and not return until a button is clicked.*/
        void executeFrame(sf::RenderWindow& window,const sf::Time& deltaTime);
        //! Constructor.
        /*! Does not do anything for this class. */
        AboutUs() {};
        //! Destructor.
        /*! Destroy the button that make the scene. */
        virtual ~AboutUs();

    private:
        //! The back button.
        /*! Return to the MainMenu when clicked. */
        Button *_returnButton;
        //! The credit font.
        /*! The font used by the credit text. */
        sf::Font _CreditFont;
        //! The credit text.
        /*! The text that give details about the game. */
        sf::Text _CreditText;

        //! Handle a click on the screen.
        /*! Handle the users click on the screen, and execute the right action if a button is clicked. */
        bool handleClick(const int x,const int y);
};

#endif // ABOUTUS_H
